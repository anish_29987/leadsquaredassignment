﻿using CardGame.Deck.Cards;
using CardGame.Deck.Helper;
using CardGame.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Deck
{
    /// <summary>
    /// Represents a Deck of Cards
    /// </summary>
    public class Deck: IDeck
    {
        #region Private Data Members
        /// <summary>
        /// List of Cards
        /// </summary>
        private List<string> deckOfCards;

        /// <summary>
        /// Satrting counter for tracking cards removed
        /// </summary>
        private int topCounter;

        /// <summary>
        /// Maximum number of cards available in adeck at a single instance
        /// </summary>
        private int maxCount = 52;

        /// <summary>
        /// Cards of type Diamonds
        /// </summary>
        private readonly CardsBase diamonds;

        /// <summary>
        /// Cards of type Hearts
        /// </summary>
        private readonly CardsBase hearts;

        /// <summary>
        /// Cards of type Clubs
        /// </summary>
        private readonly CardsBase clubs;

        /// <summary>
        /// Cards of type Spades
        /// </summary>
        private readonly CardsBase spades;
        #endregion

        #region Construtor
        /// <summary>
        /// Constructor
        /// </summary>
        public Deck()
        {
            diamonds = new Diamonds();
            hearts = new Hearts();
            clubs = new Clubs();
            spades = new Spades();
            ResetDeck();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the Top most card from the Deck. 
        /// If we have reached end of the deck we will get null.
        /// </summary>
        /// <returns>nullable string</returns>
        public string? GetTopCardFromADeck()
        {         
            return PopACard();
        }

        /// <summary>
        /// Shuffles the deck of cards on a random fashion.
        /// </summary>
        /// <param name="numberOfTimes"> No of times we want to shuffle the deck.
        /// Default value is one</param>
        public void Shuffle (int numberOfTimes= 1)
        {
            for (int i = 0; i< numberOfTimes; i++)
            {
                deckOfCards.Shuffle();
            }
        }

        /// <summary>
        /// Rests the Deck of cards to intial state with a random Shuffle
        /// </summary>
        public void ResetDeck ()
        {
            deckOfCards = new List<string>();
            deckOfCards.AddRange(diamonds.Cards.Values);
            deckOfCards.AddRange(hearts.Cards.Values);
            deckOfCards.AddRange(clubs.Cards.Values);
            deckOfCards.AddRange(spades.Cards.Values);
            Shuffle();
            topCounter = 0;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Remove card from top of the list
        /// </summary>
        /// <returns></returns>
        private string? PopACard()
        {
            if (topCounter == maxCount)
            {
                return null;
            }
            var card = deckOfCards.First();
            deckOfCards.RemoveAt(0);
            topCounter += 1;
            return card;
        }
        #endregion
    }
}
