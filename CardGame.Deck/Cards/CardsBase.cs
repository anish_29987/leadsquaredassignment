﻿using System.Collections.Generic;

namespace CardGame.Deck.Cards
{
    /// <summary>
    /// Base Class represting Individual Cards
    /// </summary>
    public abstract class CardsBase
    {
        #region Internal Data Members
        /// <summary>
        /// List of Cards
        /// </summary>
        internal Dictionary<int, string> Cards;
        #endregion

        #region Private Data Memebers
        /// <summary>
        /// Card Pattern
        /// </summary>
        private readonly string displayPattern = "{0}         \n           \n           \n     {1}     \n           \n           \n         {2}";
        #endregion

        #region Construtor
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="cardSign"></param>
        internal CardsBase (string cardSign)
        {
            Cards = new Dictionary<int, string>();
            for (int i = 1; i<=13; i++)
            {
                string cardDesign;
                switch (i)
                {
                    case 1:
                        cardDesign = string.Format(displayPattern, cardSign, "A", cardSign);
                        break;
                    case 11:
                        cardDesign = string.Format(displayPattern, cardSign, "J", cardSign);
                        break;
                    case 12:
                        cardDesign = string.Format(displayPattern, cardSign, "Q", cardSign);
                        break;
                    case 13:
                        cardDesign = string.Format(displayPattern, cardSign, "K", cardSign);
                        break;
                    default:
                        cardDesign = string.Format(displayPattern, cardSign, i, cardSign);
                        break;

                }
                Cards.Add(i, cardDesign);
            }

        }
        #endregion
    }

}
