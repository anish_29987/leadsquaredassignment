﻿namespace CardGame.Deck.Cards
{
    /// <summary>
    /// Representation of Clubs
    /// </summary>
    public class Clubs: CardsBase
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Clubs(): base("♣")
        {

        }
        #endregion
    }
}