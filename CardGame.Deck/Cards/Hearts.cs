﻿namespace CardGame.Deck.Cards
{
    /// <summary>
    /// Representation of hearts
    /// </summary>
    public class Hearts : CardsBase
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Hearts() : base("♥")
        { 

        }
        #endregion
    }
}
