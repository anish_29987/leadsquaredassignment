﻿namespace CardGame.Deck.Cards
{
    /// <summary>
    /// Representation of Diamonds
    /// </summary>
    public class Diamonds : CardsBase
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Diamonds() : base("♦")
        { 

        }
        #endregion
    }
}
