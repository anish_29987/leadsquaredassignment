﻿namespace CardGame.Deck.Cards
{
    /// <summary>
    /// Representation of Spades
    /// </summary>
    public class Spades : CardsBase
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Spades() : base("♠")
        {

        }
        #endregion
    }
}
