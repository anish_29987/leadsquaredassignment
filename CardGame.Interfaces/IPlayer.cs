﻿namespace CardGame.Interfaces
{
    /// <summary>
    /// User Contract who inputs the choice of move
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Selected move by the user.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        bool Move(string input);
    }
}
