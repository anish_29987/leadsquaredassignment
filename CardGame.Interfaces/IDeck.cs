﻿namespace CardGame.Interfaces
{
    /// <summary>
    /// Contract Defining teh operations to be performed on the Deck
    /// </summary>
    public interface IDeck
    {
        /// <summary>
        /// Gets the card from the top of the Deck
        /// </summary>
        /// <returns></returns>
        public string? GetTopCardFromADeck();

        /// <summary>
        /// Shuffles the deck a given no of times.
        /// </summary>
        /// <param name="numberOfTimes"></param>
        public void Shuffle(int numberOfTimes = 1);

        /// <summary>
        /// Resets deck to its Initial State
        /// </summary>
        public void ResetDeck();
    }
}
