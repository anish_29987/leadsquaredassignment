﻿namespace CardGame.Interfaces
{
    /// <summary>
    /// Service Contract Entry point to the game.
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Starts the execution of the Application.
        /// </summary>
        void Execute();
    }
}
