﻿namespace CardGame.Interfaces
{
    /// <summary>
    /// Contract to establish user palying moves for the user
    /// </summary>
    public interface IDealer
    {
        /// <summary>
        /// Get the next card from the Deck
        /// </summary>
        /// <returns></returns>
        string GetNextHand();

        /// <summary>
        /// Shuffles the existing cards in the deck.
        /// </summary>
        void ShuffleDeck(int numberOfTimes = 1);

        /// <summary>
        /// Restarts game.
        /// </summary>
        void RestartGame();
    }
}
