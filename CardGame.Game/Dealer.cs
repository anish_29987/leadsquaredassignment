﻿using CardGame.Interfaces;

namespace CardGame.Game
{
    /// <summary>
    /// Represents a Dealer who is resposible for dealing card from the Deck.
    /// </summary>
    public class Dealer : IDealer
    {
        #region Private Memebers
        /// <summary>
        /// deck of cards
        /// </summary>
        private readonly IDeck deck;
        #endregion

        #region Public Constructor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="deck"></param>
        public Dealer(IDeck deck)
        {
            this.deck = deck;
        }
        #endregion

        #region Public Memebers
        /// <summary>
        /// Deals the next Card
        /// </summary>
        /// <returns></returns>
        public string GetNextHand()
        {
            var value = deck.GetTopCardFromADeck();
            if(value == null)
            {
                return "Deck is Empty please restart game.";
            }
            return value.ToString();
        }

        /// <summary>
        /// Resets the deck
        /// </summary>
        public void RestartGame()
        {
            deck.ResetDeck();
        }

        /// <summary>
        ///  Shuffle the card in the deck
        /// </summary>
        /// <param name="numberOfTimes"></param>
        public void ShuffleDeck(int numberOfTimes = 1)
        {
            deck.Shuffle();
        }
        #endregion
    }
}
