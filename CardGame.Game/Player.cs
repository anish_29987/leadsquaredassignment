﻿using CardGame.Interfaces;
using System;

namespace CardGame.Game
{
    /// <summary>
    /// Class Representing a user playing the game
    /// </summary>
    public class Player : IPlayer
    {
        #region Private Members
        /// <summary>
        /// Deals card to the player from the Deck
        /// </summary>
        private readonly IDealer play;

        /// <summary>
        /// Player Options Message
        /// </summary>
        private readonly string playerOptions =" Press 2 to Exit \n Press 3 to Restart \n Press 4 Get Next Card \n Press 5 to Shuffle \n";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="play"></param>
        public Player(IDealer play)
        {
            this.play = play;
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Payer Next Move
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool Move(string input)
        {
            switch(input)
            {
                case "1":
                    //Starting the game and Disaplayinng the First Card
                    Console.WriteLine(playerOptions);
                    Console.WriteLine(play.GetNextHand());
                    return true;
                case "2":
                    //Exit from the Game
                    return false; 
                case "3":
                    //Restarting the game
                    play.RestartGame();
                    Console.Clear();
                    Console.WriteLine(playerOptions);
                    Console.WriteLine(play.GetNextHand());
                    return true; 
                case "4":
                    // Geting the next card from the deck
                    Console.WriteLine(playerOptions);
                    Console.WriteLine(play.GetNextHand());
                    return true; 
                case "5":
                    //Shuffling the Deck of remaining cards
                    play.ShuffleDeck();
                    Console.WriteLine(playerOptions);
                    Console.WriteLine("Deck Shuffled Successfully");
                    return true; 
                default:
                    // Wrong input Handling
                    Console.WriteLine("Wrong Input. Please Try with the Correct Input. \n");
                    Console.WriteLine(playerOptions);
                    return true;
            }
        }
        #endregion
    }
}
