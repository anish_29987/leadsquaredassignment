﻿using CardGame.Interfaces;
using System;

namespace CardGame.Game
{
    /// <summary>
    /// Service Implementation of the Application.
    /// Which Acts as the entry poit to the Game.
    /// </summary>
    public class Service : IService
    {
        #region Private Data Memebers
        /// <summary>
        /// Instance of the Player Playing the game
        /// </summary>
        private readonly IPlayer player;

        /// <summary>
        /// Tracks player executions
        /// </summary>
        private bool excutionContinue = true;
        #endregion

        #region Constructor
        public Service (IPlayer player)
        {
            this.player = player;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Starts Executing the game
        /// </summary>
        public void Execute()
        {
            try
            {
                Console.WriteLine("Welcome to Card Game \n");
                Console.WriteLine("Press 1 to Start \n");
                Console.WriteLine("Press 2 to Exit \n");
                while (excutionContinue)
                {
                    var input = Console.ReadLine();
                    excutionContinue = player.Move(input);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("\n Press any Key to Exiit");
                Console.ReadLine();
            }
        }
        #endregion
    }
}
