﻿using CardDeck = CardGame.Deck;
using CardGame.Game;
using CardGame.Interfaces;
using System;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var serviceProvider = new ServiceCollection()
            .AddSingleton<IDeck, CardDeck.Deck>()
            .AddSingleton<IDealer, Dealer>()
            .AddSingleton<IPlayer, Player>()
            .AddSingleton<IService, Service>()
            .BuildServiceProvider();

            var service = serviceProvider.GetService<IService>(); 
            service.Execute();
        }
    }
}
